# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 1999-01-12 01:14+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: LinuxKorea Co. <kde@linuxkorea.co.kr>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8BIT\n"

#: main.cpp:84
msgid "&Appearance"
msgstr "형태(&A)"

#: main.cpp:92
msgid "&Background"
msgstr "배경(&B)"

#: main.cpp:95
msgid "&Users"
msgstr "사용자(&U)"

#: main.cpp:98
msgid "&Sessions"
msgstr "세션(&S)"

#: main.cpp:103
msgid ""
"usage: kdmconfig [-init | {appearance,font,background,sessions,users}]\n"
msgstr "사용법: kdmconfig [-init | {형태,글꼴,백그라운드,세션,사용자}]\n"

#: main.cpp:142
msgid "KDM Configuration"
msgstr "KDM 설정"

#: main.cpp:152
msgid ""
"Sorry, but you don't have read/write\n"
"permission to the KDM setup file."
msgstr ""
"귀하는 KDM 설정 파일에 \n"
"읽기쓰기 권한이 없습니다."

#: main.cpp:154
msgid "Missing privileges"
msgstr "권한 없음"

#: kdm-appear.cpp:70
msgid "Greeting string:"
msgstr "환영 메시지:"

#: kdm-appear.cpp:78
msgid "KDM logo:"
msgstr "KDM 로고:"

#: kdm-appear.cpp:105 kdm-users.cpp:135
msgid "Click or drop an image here"
msgstr "클릭 또는 이미지를 넣으십시오"

#: kdm-appear.cpp:110
msgid "GUI Style:"
msgstr "GUI 스타일:"

#: kdm-appear.cpp:116
msgid "Motif"
msgstr "모티프"

#: kdm-appear.cpp:117
msgid "Windows"
msgstr "윈도우"

#: kdm-appear.cpp:128
msgid "Language"
msgstr "언어"

#: kdm-appear.cpp:131
msgid "Language:"
msgstr "언어:"

#: kdm-appear.cpp:167 kdm-appear.cpp:251 kdm-users.cpp:276
msgid ""
"There was an error loading the image:\n"
">"
msgstr ""
"이미지 로딩중 오류:\n"
">"

#: kdm-appear.cpp:169 kdm-appear.cpp:188 kdm-users.cpp:196
msgid "<"
msgstr "<"

#: kdm-appear.cpp:170 kdm-appear.cpp:189 kdm-appear.cpp:254 kdm-users.cpp:197
#: kdm-users.cpp:279
msgid "ERROR"
msgstr "오류"

#: kdm-appear.cpp:186 kdm-users.cpp:194
msgid ""
"There was an error saving the image:\n"
">"
msgstr ""
"이미지 저장 중 오류가 발생했읍니다:\n"
">"

#: kdm-appear.cpp:219 kdm-users.cpp:227
msgid "Sorry, but \n"
msgstr "죄송합니다 \n"

#: kdm-appear.cpp:221 kdm-users.cpp:229
msgid ""
"\n"
"does not seem to be an image file"
msgstr ""
"\n"
"이미지 파일이 아닌 것 같습니다"

#: kdm-appear.cpp:222 kdm-users.cpp:230
msgid ""
"\n"
"Please use files with these extensions\n"
msgstr ""
"\n"
"이 확장자와 함께 파일을 사용해주십시오\n"

#: kdm-appear.cpp:224 kdm-users.cpp:232
msgid "Improper File Extension"
msgstr "부적절한 파일 확장자"

#: kdm-appear.cpp:253 kdm-users.cpp:278
msgid ""
"<\n"
"It will not be saved..."
msgstr ""
"<\n"
"저장하지 않습니다..."

#: kdm-font.cpp:48
msgid "Select fonts"
msgstr "글꼴 선택"

#: kdm-font.cpp:50
#, fuzzy
msgid "Example"
msgstr "예제"

#: kdm-font.cpp:52
#, fuzzy
msgid "Change font..."
msgstr "글꼴 변경"

#: kdm-font.cpp:57
msgid "Greeting"
msgstr "환영"

#: kdm-font.cpp:58
msgid "Fail"
msgstr "실패"

#: kdm-font.cpp:59
msgid "Standard"
msgstr "표준"

#: kdm-font.cpp:157
#, fuzzy
msgid "Greeting font"
msgstr "환영"

#: kdm-font.cpp:161
#, fuzzy
msgid "Fail font"
msgstr "실패"

#: kdm-font.cpp:165
#, fuzzy
msgid "Standard font"
msgstr "표준"

#: kdm-bgnd.cpp:70
msgid "Preview"
msgstr ""

#: kdm-bgnd.cpp:95
msgid "Color"
msgstr "색상"

#: kdm-bgnd.cpp:101
#, fuzzy
msgid "Solid Color"
msgstr "색상"

#: kdm-bgnd.cpp:106
msgid "Horizontal Blend"
msgstr ""

#: kdm-bgnd.cpp:111
msgid "Vertical Blend"
msgstr ""

#: kdm-bgnd.cpp:130
msgid "Wallpaper"
msgstr "바탕화면"

#: kdm-bgnd.cpp:171 kdm-sess.cpp:56
msgid "None"
msgstr ""

#: kdm-bgnd.cpp:176
#, fuzzy
msgid "Tile"
msgstr "타일형식"

#: kdm-bgnd.cpp:181
#, fuzzy
msgid "Center"
msgstr "중앙"

#: kdm-bgnd.cpp:186
#, fuzzy
msgid "Scale"
msgstr "크기조절"

#: kdm-bgnd.cpp:191
msgid "TopLeft"
msgstr ""

#: kdm-bgnd.cpp:196
msgid "TopRight"
msgstr ""

#: kdm-bgnd.cpp:201
msgid "BottomLeft"
msgstr ""

#: kdm-bgnd.cpp:206
msgid "BottomRight"
msgstr ""

#: kdm-bgnd.cpp:211
msgid "Fancy"
msgstr ""

#: kdm-users.cpp:48
msgid "All users"
msgstr "모든 사용자"

#: kdm-users.cpp:50
msgid "Selected users"
msgstr "선택된 사용자"

#: kdm-users.cpp:52
msgid "No-show users"
msgstr "보여주지 말아야할 사용자"

#: kdm-users.cpp:79
msgid ""
"Show only\n"
"selected users"
msgstr ""
"선택된 사용자만\n"
"보여주기"

#: kdm-users.cpp:86
msgid ""
"Show all users\n"
" but no-show users"
msgstr ""
"보여주지 말아야 할 사용자를 제외한\n"
"모든 사용자에게 보여주기"

#: kdm-users.cpp:97
msgid "Show users"
msgstr "사용자 보기"

#: kdm-users.cpp:103
msgid "Sort users"
msgstr "사용자 분류"

#: kdm-users.cpp:182 kdm-users.cpp:251
msgid "No user selected"
msgstr "사용자가 선택되지 않음"

#: kdm-users.cpp:183 kdm-users.cpp:252
msgid "Save image as default image?"
msgstr "이 이미지를 디폴트 이미지로 저장하시겠습니까?"

#: kdm-sess.cpp:52
msgid "Allow to shutdown"
msgstr ""

#: kdm-sess.cpp:57
msgid "All"
msgstr "모든 사용자"

#: kdm-sess.cpp:58
msgid "Root Only"
msgstr "루트만"

#: kdm-sess.cpp:59
msgid "Console Only"
msgstr "콘솔에서만"

#: kdm-sess.cpp:63
msgid "Commands"
msgstr "명령"

#: kdm-sess.cpp:64
msgid "Shutdown"
msgstr "셧다운"

#: kdm-sess.cpp:70
msgid "Restart"
msgstr "재실행"

#: kdm-sess.cpp:77
msgid "Session types"
msgstr "세션 유형"

#: kdm-sess.cpp:79
msgid "New type"
msgstr "새 유형"

#: kdm-sess.cpp:89
msgid "Available types"
msgstr "가능한 유형"

#: klangcombo.cpp:133
msgid "without name!"
msgstr "이름 없음!"
